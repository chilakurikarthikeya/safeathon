import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';

import {RouterModule} from '@angular/router';

import {AuthService1} from 'src/app/auth.service';
import { from } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './components/register/register.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { CommonModule } from '@angular/common';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';


import {AuthGuard} from './auth/auth.guard';
import { ReactiveFormsModule } from '@angular/forms';
import {AuthInterceptor} from './auth/auth.interceptor';

// social login
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from 'angular-6-social-login';

// MATERIAL DESIGN
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { AfterLoginComponent } from './components/after-login/after-login.component';
import { EventsComponent } from './components/events/events.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

// file upload
// import { FileUploadModule } from 'ng2-file-upload';
// import { FileSelectDirective } from 'ng2-file-upload';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';


// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
	      provider: new FacebookLoginProvider('390441045028519')
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
	      provider: new GoogleLoginProvider('179012507347-vo9607m9sq495kqsrc83u2fbh2kk8gos.apps.googleusercontent.com')
        },
          {
            id: LinkedinLoginProvider.PROVIDER_ID,
            provider: new LinkedinLoginProvider('1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com')
          },
      ],
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    AfterLoginComponent,
    EventsComponent
  
    // MatFormFieldModule
    // AuthService


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    // fire base

    AngularFireModule.initializeApp({
      apiKey: 'IzaSyAkxllhBOOrmEIVFOdEJGF-PFVR-Ez5Y08',
      authDomain: 'safeathon.firebaseapp.com',
      storageBucket: 'safeathon.appspot.com',
      projectId: 'safeathon',
    }),
    AngularFireStorageModule,

    RouterModule.forRoot([
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      {path: 'home', component: HomeComponent},
      {path: 'login', component: LoginComponent },
      {path: 'register', component: RegisterComponent},
      {path: 'after_login', component: AfterLoginComponent, canActivate: [AuthGuard]},
      {path: 'events', component: EventsComponent, canActivate: [AuthGuard]}
    ]),
    // RouterModule.forRoot(routes, {onSameUrlNavigation: ‘reload’}),
    BrowserAnimationsModule, ReactiveFormsModule, SocialLoginModule, CommonModule,
    MatFormFieldModule, MatSelectModule, MatInputModule, MatIconModule, MatButtonModule, MatGridListModule,
    MatProgressSpinnerModule
  ],
  providers: [AuthService1, AuthGuard ,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';

import {Http, Headers} from '@angular/http';

// import {HttpClient} from '@angular/common/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
// import 'rxjs/add/add/operator/map';
import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';
// import { map } from 'rxjs';
import {Router} from '@angular/router';
// import {tokenNotExpired} from 'angular2-jwt';
import {environment} from '../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AuthService1 {
  authToken: any;
  user: any;
  public user_email: any;
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
  constructor( private http: HttpClient, private router: Router) { }



  register(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post( 'https://tranquil-ridge-81301.herokuapp.com/users/register', user, this.noAuthHeader);
    // return this.http.post( 'http://localhost:8080/users/register', user, this.noAuthHeader);
    }


    login(user) {
       this.user_email = user['mail'];
      // alert(user['email']);
         return this.http.post('https://tranquil-ridge-81301.herokuapp.com/users/authenticate', user, this.noAuthHeader);
    }


    getevents() {
      return this.http.get('https://tranquil-ridge-81301.herokuapp.com/users/user_profile');
    }

    getUserPfofile() {
      console.log('user callrd');
      return this.http.get( 'https://tranquil-ridge-81301.herokuapp.com/users/user_profile');
    }

   update(method) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
      return this.http.put( 'https://tranquil-ridge-81301.herokuapp.com/users/update', method);
        // return this.http.put( 'http://localhost:8080/users/update', method);

    }







setToken(token: string) {
  localStorage.setItem('token', token);
}
getToken() {
  return localStorage.getItem('token');
}

deleteToken() {
  localStorage.removeItem('token');
}

getUserPayload() {
  var token= this.getToken();
  if (token) {
    var userPayload = atob(token.split('.')[1]);
    console.log(userPayload);
    return JSON.parse(userPayload);
  } else {
    return null;
  }
}
isLoggedIn() {
 
  var userPayload = this.getUserPayload();
  if (userPayload) {
    console.log(' IsLOgged()');
    console.log(userPayload.exp > Date.now() / 1000);
    return userPayload.exp > Date.now() / 1000;
  } else {
    console.log('no login');
    return false;
  }
}

// loggedIn() {
//   return tokenNotExpired();
// }

}

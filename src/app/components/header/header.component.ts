import { Component, OnInit } from '@angular/core';
import {AuthService1} from 'src/app/auth.service';
import { NgModule,  } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
} from '@angular/router';


// social login
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = 'Ng-Teams';
  loading = false;

  name: String;
  public email: String;
  sex: String;
  constructor( public authService: AuthService1, private router: Router, private socialAuthService: AuthService) { 
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }


  ngOnInit() {
  }

  logout() {
    this.authService.deleteToken();
    this.router.navigateByUrl('/home');
  }


  public socialSignIn(socialPlatform: string) {
    if ( this.authService.isLoggedIn()) {
      alert('alreay loggedin');
      return  this.router.navigateByUrl('/home');

    }
    let socialPlatformProvider;
    if (socialPlatform == 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }// else if (socialPlatform == 'linkedin') {
    //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    // }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        // json({ "token": token });
        this.email = userData['email'];
        console.log(userData['email']);
        const user = {
          email: userData['email'],
          password: ' '
        };

        this.authService.login(user).subscribe(
          res => {
            console.log(res['token']);
            this.authService.setToken(res['token']);
            window.location.reload();
            this.router.navigate(['/home']);
          },
          err => {
            localStorage.setItem('mail', userData['email'] );
            this.router.navigateByUrl('/register');
          }
        );

        console.log(socialPlatform + ' sign in data : ' , userData);
        // Now sign-in with userData
        // ...

      }
    );
  }

}

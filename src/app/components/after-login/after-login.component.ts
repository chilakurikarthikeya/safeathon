import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AuthService1} from 'src/app/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.css']
})
export class AfterLoginComponent implements OnInit {
  model = {
    email: '',
  };
  constructor(private authService: AuthService1, private router: Router) { }

  serverErrorMessages: String;

  ngOnInit() {
  }

  // onSubmit(form: NgForm ) {
  //   this.usersService.login(form.value).subscribe(
  //     res => {

  //       this.router.navigateByUrl('/register');
  //       console.log(form.value);
  //     },
  //     err => {
  //         this.serverErrorMessages = err.console.error.message;
  //         console.log(form.value);

  //     }
  //   );
  // }
}

import { Component, OnInit } from '@angular/core';

import {AuthService1} from 'src/app/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl, NgForm } from '@angular/forms';
// import { ActivatedRoute,  Routes } from '@angular/router';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registredForm: FormGroup;
  submitted = false;
  registerForm;

  constructor( private authService: AuthService1, private router: Router, private formBuilder: FormBuilder,
      private route: ActivatedRoute) { }
  model = {
    email: localStorage.getItem('mail')
  };

  ngOnInit() {

    // const id = this.route.snapshot.paramMap.get('id');

    if ( this.authService.isLoggedIn()) {
      // alert('login');
      return  this.router.navigateByUrl('/home');

    }

    console.log(this.route.snapshot.paramMap.get('id'));
    this.registerForm = this.formBuilder.group({
      Name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      ph_no: ['', [Validators.required, Validators.minLength(10)]],
      country: ['', [Validators.required]],
      state: ['', Validators.required ],
      city: ['', Validators.required],
      college: ['', Validators.required]
  });

}
get f() { return this.registerForm.controls; }

onSubmit() {
  this.submitted = true;
  

  // stop here if form is invalid
  if (this.registerForm.invalid) {
    console.log('failure');
    // alert('d');
      return;
  }

    this.authService.register(this.registerForm.value).subscribe(
      res => {

        alert('Registred successfully');
        this.router.navigateByUrl('/home');
      },
      err => {
        if (err.status === 422) {
          alert('Email already exists');
        } else {
          console.log(err);
        }

      }
    );
  console.log(this.registerForm.value);
}

}

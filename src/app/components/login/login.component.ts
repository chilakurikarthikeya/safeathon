import { Component, OnInit } from '@angular/core';
import {AuthService1} from 'src/app/auth.service';
import { NgModule,  } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormControl , FormBuilder, NgForm} from '@angular/forms';

// social login
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name: String;
  public email: String;
  sex: String;
  constructor( private authService: AuthService1, private router: Router, private socialAuthService: AuthService) { }

  ngOnInit() {
    if ( this.authService.isLoggedIn()) {
      alert('login');
      return  this.router.navigateByUrl('/home');

    }
  }

  public socialSignIn(socialPlatform: string) {
    if ( this.authService.isLoggedIn()) {
      alert('login');
      return  this.router.navigateByUrl('/home');

    }
    let socialPlatformProvider;
    if (socialPlatform == 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }// else if (socialPlatform == 'linkedin') {
    //   socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    // }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        // json({ "token": token });
        this.email = userData['email'];
        console.log(userData['email']);
        const user = {
          email: userData['email'],
          password: ' '
        };

        this.authService.login(user).subscribe(
          res => {
            console.log(res['token']);
            this.authService.setToken('cool:-------------' + res['token']);
            this.router.navigateByUrl('/home');
          },
          err => {
            this.router.navigate(['/register/' + this.email]);
          }
        );

        console.log(socialPlatform + ' sign in data : ' , userData);
        // Now sign-in with userData
        // ...

      }
    );
  }






  // onRegisterSubmit() {
  //  const user = {
  //    name: this.name,
  //    email: this.email,
  //    sex : this.sex

  //  };
  // //  console.log(user);
  //  this.authService.register(user).subscribe(data => {
  //   if (data.success) {
  //     console.log('reg success');
  //     this.router.navigate(['/home']);
  //   } else {
  //     console.log('reg failure');
  //     this.router.navigate(['/login']);
  //   }
  // });
  // }

  // twitterlogin() {
  //   this.authService.twitterlogin();
  //   // .subscribe(
  //   //   res => {
  //   //     this.authService.setToken(res['token']);
  //   //     console.log(res['token']);
  //   //   },
  //   //   err => {
  //   //       console.log('not getting token');
  //   //   }
  //   // );
  // }

}

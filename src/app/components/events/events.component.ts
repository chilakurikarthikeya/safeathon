import { Component, OnInit } from '@angular/core';
import {AuthService1} from 'src/app/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl, NgForm } from '@angular/forms';


// file upload

import {HttpClient} from '@angular/common/http';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
// import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { Attribute } from '@angular/compiler';
import { map } from 'rxjs/operators/map';
const URL = 'http://localhost:8080/users/upload';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  UserDetails;
  MyIdea;
  update;
  selectedFile = null;
  // file upload
  title = 'app';
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  uploadState: Observable<string>;
  link: FormGroup;
  name;


  ref2: AngularFireStorageReference;
  task2: AngularFireUploadTask;
  uploadProgress2: Observable<number>;
  downloadURL2: Observable<string>;
  uploadState2: Observable<string>;
  
  constructor( private authService: AuthService1, private router: Router, private http: HttpClient,
    private afStorage: AngularFireStorage, private formBuilder: FormBuilder) { }




  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});

  model: any = {};

  ngOnInit() {
    this.authService.getUserPfofile().subscribe(
      res => {
          this.UserDetails = res['user'];
          console.log(this.UserDetails);
          console.log('karthik');
          this.MyIdea = this.UserDetails.events.MyIdea_reg;

      },
      err => {
        console.log('NO DATA');
      }
    );

    // file upload
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
         alert('File uploaded successfully');
     };


      // getting reg link
    // this.link = this.formBuilder.group({
    //   name: ['', Validators.required],
    // });
 }


 get f() { return this.link.controls; }


// getting reg  link

get_link(event) {
  // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.model));
  // alert(this.model.link);
  if (event == 1) {
    // alert(event);
    this.onupdate(event, this.model.link1);

  }
  if (event == 2) {
    // alert(event);
    this.onupdate(event, this.model.link2);
  }
  if (event == 3) {
    this.onupdate(event, this.model.link3);
  }
  if (event == 5) {
    this.onupdate(event, this.model.link4);
  }
  if (event == 7) {
    this.onupdate(event, this.model.link5);
  }



}

  onupdate(event, reg) {
      this.update = {
          event: event,
          opt: reg
      };
      // alert(this.model.link1);
      // alert(event);
      this.authService.update(this.update).subscribe(
        res => {
            alert('Successfull');
            window.location.reload();
            this.router.navigateByUrl('/events');
        },
        err => {
          alert('error');
        }
      );
  }



  upload(event) {
    // const id = Math.random().toString(36).substring(2);

      this.name = 'myidea =';

    const id =  this.name + this.UserDetails.email;
    this.ref = this.afStorage.ref(id);
    this.task = this.ref.put(event.target.files[0]);

    this.uploadProgress = this.task.percentageChanges();
    // console.log( this.task.percentageChanges());
    this.downloadURL = this. ref.getDownloadURL();

  }

  upload2(event) {
    // const id = Math.random().toString(36).substring(2);

      this.name = 'anyalysis=';
    const id2 =  this.name + this.UserDetails.email;
    this.ref2 = this.afStorage.ref(id2);
    this.task2 = this.ref2.put(event.target.files[0]);

    this.uploadProgress2 = this.task2.percentageChanges();
    // console.log( this.task.percentageChanges());
    this.downloadURL2 = this. ref2.getDownloadURL();

  }



}
